### Fewygram

This application is a replica of the famous Instagram app. To start this app, a Firebase account must be set up. In order for the app to work, a google-services.json with all the necessary Firebase information must be created in the /app directory.