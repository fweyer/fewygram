package de.fewycoding.fewygram.network.firebase.database

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import de.fewycoding.fewygram.domain.DurrationEnum
import de.fewycoding.fewygram.domain.StoryDto
import de.fewycoding.fewygram.domain.UserStoryDto
import java.util.HashMap

object FirebaseStoryService {

    private val firebaseDatabase = FirebaseDatabase.getInstance()

    fun saveStory(storyDtoInfo: StoryDto, onSuccess: () -> Unit, onError: (message: String) -> Unit) {
        if (storyDtoInfo.userId == null || storyDtoInfo.imageURL == null){
            onError("Storyupload nicht möglich")
            return
        }
        val storyRef = firebaseDatabase.reference.child("storys")
        val storyId = storyRef.push().key
        val storyMap = HashMap<String,Any>()
        storyId?.let {
            storyMap["storyId"] = storyId
            storyMap["userId"] = storyDtoInfo.userId!!
            storyMap["imageURL"] = storyDtoInfo.imageURL!!
            storyMap["timeEnd"] = System.currentTimeMillis() + getDuration(DurrationEnum.ONE_DAY)

            storyRef.child(storyId).setValue(storyMap).addOnCompleteListener { task ->
                if (task.isSuccessful)
                    onSuccess()
                else
                    onError(task.exception.toString())
            }
        }
    }

    fun fetchStorys(onComplition: (storyDtos : List<UserStoryDto>) -> Unit){
        val storyref = firebaseDatabase.reference.child("storys")
        val storyList = mutableListOf<StoryDto>()
        FirebaseFollowService.fetchCurrentUserFollowingList { followingList ->
            storyref.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    snapshot.children.forEach {
                        val story = it.getValue(StoryDto::class.java)
                        for (id in followingList) {
                            if (!filterStory(story,id)) {
                                storyList.add(story!!)
                            }
                        }
                    }
                    val userStoryList = mapStorysToUserStoryList(storyList)
                    onComplition(userStoryList.reversed())
                }
                override fun onCancelled(error: DatabaseError) {
                    return
                }
            })
        }
    }


    fun deleteStory(storyDto: StoryDto?, onSuccess: () -> Unit, onError: (message: String) -> Unit) {
        if (storyDto?.storyId == null) {
            onError("Story konnte nicht gelöscht werden")
            return
        }

        firebaseDatabase.reference.child("storys").child(storyDto.storyId!!).removeValue().addOnCompleteListener { task ->
               if (task.isSuccessful)
                   onSuccess()
               else
                   onError("Story konnte nicht gelöscht werden")
            }
    }

    private fun mapStorysToUserStoryList(storyDtos : List<StoryDto>) : List<UserStoryDto>{
        val userStorys = mutableListOf<UserStoryDto>()
        val userSet  = mutableSetOf<String>()
        storyDtos.forEach { it.userId?.let { userid -> userSet.add(userid) } }
        userSet.forEach {
            val storyList = mutableListOf<StoryDto>()
            for (story in storyDtos){
                if (story.userId == it)
                    storyList.add(story)
            }
            val userStory = UserStoryDto(it, storyList)
            userStorys.add(userStory)
        }
        return userStorys
    }

    private fun filterStory(storyDto : StoryDto?, id: String) : Boolean{
        if (storyDto?.timeEnd == null)
            return true

        val currentTime = System.currentTimeMillis()

        return storyDto.userId != id ||
               storyDto.timeEnd!! < currentTime
    }


    private fun getDuration(durrationEnum: DurrationEnum) : Long{
        return when(durrationEnum){
            DurrationEnum.ONE_DAY -> 86400000
            DurrationEnum.ONE_HOUR -> (86400000/24)
            DurrationEnum.ONE_MINUTE -> (86400000/24/60)
        }
    }


}