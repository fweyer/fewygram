package de.fewycoding.fewygram.network.firebase.database

import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import de.fewycoding.fewygram.domain.PostDto
import de.fewycoding.fewygram.network.firebase.auth.FirebaseAuthService
import de.fewycoding.fewygram.network.firebase.storage.FirebaseStorageService
import java.util.*

object FirebasePostService {

    private val firebaseDatabase = FirebaseDatabase.getInstance()

    fun savePost(post : PostDto, onSuccess: () -> Unit, onError: (message: String) -> Unit){
        val postRef = firebaseDatabase.reference.child("posts")
        val postID = postRef.push().key
        val postMap = HashMap<String,Any>()
        postMap["postID"] = postID!!
        postMap["description"] = post.description!!
        postMap["publisher"] = post.publisher!!
        postMap["postImageURL"] = post.postImageURL!!

        postRef.child(postID).setValue(postMap).addOnCompleteListener { task ->
            if (task.isSuccessful)
                onSuccess()
            else
                onError(task.exception.toString())
        }
    }

    fun fetchPost(onComplition: (post : List<PostDto>) -> Unit){
       val postref = firebaseDatabase.reference.child("posts")
       val postList = mutableListOf<PostDto>()
       FirebaseFollowService.fetchCurrentUserFollowingList { followingList ->
           postref.addValueEventListener(object : ValueEventListener {
               override fun onDataChange(snapshot: DataSnapshot) {
                   snapshot.children.forEach {
                       val post = it.getValue(PostDto::class.java)
                       for (id in followingList) {
                           if (post?.publisher == id) {
                               postList.add(post)
                           }
                       }
                   }
                   onComplition(postList)
               }
               override fun onCancelled(error: DatabaseError) {
                   return
               }
           })
       }
    }

    fun fetchPostFromCurrentUser(onComplition: (post : List<PostDto>) -> Unit){
        val currentUserID = FirebaseAuthService.getCurrentUser()?.uid
        if(!isCurrentUsersValid(currentUserID))
            return

        val postList = mutableListOf<PostDto>()

        firebaseDatabase.reference.child("posts").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                snapshot.children.forEach {
                    val post = it.getValue(PostDto::class.java)
                    if (post?.publisher == currentUserID)
                        postList.add(post!!)
                }
                postList.reverse()
                onComplition(postList)
            }
            override fun onCancelled(error: DatabaseError) {
                return
            }
         })
    }

    fun fetchPostFromUser(uuid: String, onComplition: (post : List<PostDto>) -> Unit){
        val postList = mutableListOf<PostDto>()
        firebaseDatabase.reference.child("posts").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                snapshot.children.forEach {
                    val post = it.getValue(PostDto::class.java)
                    if (post?.publisher == uuid)
                        postList.add(post)
                }
                postList.reverse()
                onComplition(postList)
            }
            override fun onCancelled(error: DatabaseError) {
                return
            }
        })
    }


    private fun isCurrentUsersValid(currentUserUUID: String?) : Boolean{
        if (currentUserUUID == null){
            Log.e(javaClass.simpleName, "No User is logt in! Following not possible")
            return false
        }

        return true
    }

    fun deletePost(postDto: PostDto, onComplition: () -> Unit) {
        postDto.postID?.let {
            firebaseDatabase.reference.child("posts").child(it).removeValue().addOnSuccessListener {
                onComplition()
            }
        }
    }

}