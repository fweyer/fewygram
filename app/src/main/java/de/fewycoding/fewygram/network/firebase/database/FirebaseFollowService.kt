package de.fewycoding.fewygram.network.firebase.database

import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import de.fewycoding.fewygram.network.firebase.auth.FirebaseAuthService

object FirebaseFollowService {

    private val firebaseDatabase = FirebaseDatabase.getInstance()

    fun checkFollingStatus(uuidToFollow: String?, isFollowing: () -> Unit, isNotFollowing: () -> Unit){
        if (!isUserToFollowValid(uuidToFollow))
            return

        val currentUserID = FirebaseAuthService.getCurrentUser()?.uid

        if (!isCurrentUsersValid(currentUserID))
            return

        val followingRef = currentUserID.let {
            firebaseDatabase.reference
                .child("follow").child(currentUserID!!)
                .child("following")
        }

        followingRef.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.child(uuidToFollow!!).exists())
                    isFollowing()
                else
                    isNotFollowing()
            }

            override fun onCancelled(error: DatabaseError) {
                return
            }
        })
    }

    fun fetchCurrentUserFollowingList(onComplition: (followingList : List<String>) -> Unit){
        val currentUserID = FirebaseAuthService.getCurrentUser()?.uid
        if (!isCurrentUsersValid(currentUserID))
            return

        val followingList = mutableListOf<String>()

        firebaseDatabase.reference.child("follow").child(currentUserID!!).child("following")
            .get().addOnSuccessListener {
               for (snapshot in it.children){
                   snapshot.key?.let { it -> followingList.add(it) }
               }
                onComplition(followingList)
            }
    }


    fun fetchFollowingSize(userId: String, onComplition : (size : Int?) -> Unit){
        firebaseDatabase.reference.child("follow").child(userId).child("following")
            .get().addOnSuccessListener {
                val size = it.children.count()
                onComplition(size)
        }
    }

    fun fetchFollowerSize(userId: String, onComplition : (size : Int?) -> Unit){
        firebaseDatabase.reference.child("follow").child(userId).child("followers")
            .get().addOnSuccessListener {
                val size = it.children.count()
                onComplition(size)
            }
    }



    fun followOrUnfollowUser(uuidToFollow: String?, follow: Boolean, onComplition: () -> Unit) {
        if (!isUserToFollowValid(uuidToFollow))
            return

        val currentUserID = FirebaseAuthService.getCurrentUser()?.uid

        if (!isCurrentUsersValid(currentUserID))
            return

        if (follow) follow(currentUserID!!, uuidToFollow!!,onComplition)
        else unFollow(currentUserID!!, uuidToFollow!!,onComplition)

    }

    private fun follow(followUUID: String, followingUUID : String, onComplition: () -> Unit){
       firebaseDatabase.reference
            .child("follow").child(followUUID)
            .child("following").child(followingUUID)
            .setValue(true)
            .addOnCompleteListener { task ->
                if (task.isSuccessful){
                   firebaseDatabase.reference
                        .child("follow").child(followingUUID)
                        .child("followers").child(followUUID)
                        .setValue(true)
                   onComplition()
                }
            }
    }

    private fun unFollow(followUUID: String, followingUUID : String, onComplition: () -> Unit){
       firebaseDatabase.reference
            .child("follow").child(followUUID)
            .child("following").child(followingUUID)
            .removeValue()
            .addOnCompleteListener { task ->
                if (task.isSuccessful){
                    firebaseDatabase.reference
                        .child("follow").child(followingUUID)
                        .child("followers").child(followUUID)
                        .removeValue()
                    onComplition()
                }
            }
    }

    private fun isCurrentUsersValid(currentUserUUID: String?) : Boolean{
        if (currentUserUUID == null){
            Log.e(javaClass.simpleName, "No User is logt in! Following not possible")
            return false
        }

        return true
    }

    fun fetchFollowerUserIDs(userID: String, onComplition: (userIDs: MutableList<String>) -> Unit) {
        val userIds = mutableListOf<String>()
        firebaseDatabase.reference.child("follow").child(userID).child("followers").get()
            .addOnSuccessListener { snapshot ->
                snapshot.children.forEach { child ->
                    child.key?.let { userIds.add(it) }
                }
                onComplition(userIds)
            }
    }

    fun fetchFollowingUserIDs(userID: String, onComplition: (userIDs: MutableList<String>) -> Unit) {
        val userIds = mutableListOf<String>()
        firebaseDatabase.reference.child("follow").child(userID).child("following").get()
            .addOnSuccessListener { snapshot ->
                snapshot.children.forEach { child ->
                    child.key?.let { userIds.add(it) }
            }
            onComplition(userIds)
        }
    }


    private fun isUserToFollowValid(uuidToFollow: String?) : Boolean{
        if (uuidToFollow == null){
            Log.e(javaClass.simpleName, "The uuid of the Person is null! Following not possible")
            return false
        }
        return true
    }


}