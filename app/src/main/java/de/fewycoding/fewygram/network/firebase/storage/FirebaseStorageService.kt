package de.fewycoding.fewygram.network.firebase.storage

import android.net.Uri
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import de.fewycoding.fewygram.domain.PostDto
import de.fewycoding.fewygram.domain.StoryDto
import de.fewycoding.fewygram.domain.UpdateProfileImageDto
import de.fewycoding.fewygram.network.firebase.auth.FirebaseAuthService
import de.fewycoding.fewygram.network.firebase.database.FirebasePostService
import de.fewycoding.fewygram.network.firebase.database.FirebaseStoryService
import de.fewycoding.fewygram.network.firebase.database.FirebaseUserService

object FirebaseStorageService {


    private val firebaseStorage = FirebaseStorage.getInstance()


    fun uploadProfileImage(uri: Uri, onSuccess: () -> Unit, onError: (message: String) -> Unit) {
        val currentUserID = FirebaseAuthService.getCurrentUser()?.uid

        if (currentUserID == null) {
            onError("Hochladen des Bildes nicht möglich!")
            return
        }

        val fileRef = firebaseStorage.reference.child("Profile Pictures").child("$currentUserID.jpg")
        val uploadTask = fileRef.putFile(uri)
        uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
            if (task.isSuccessful)
                return@Continuation fileRef.downloadUrl
            else
                task.exception?.let { throw it }
        }).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val updateInfo = UpdateProfileImageDto(currentUserID, task.result.toString())
                FirebaseUserService.updateUserProfileImage(updateInfo, onSuccess, onError)
            } else {
                onError("Hochladen des Bildes nicht möglich!")
            }
        }
    }

    fun uploadPostImage(uri: Uri, text : String ,onSuccess: () -> Unit, onError: (message: String) -> Unit) {
        val currentUserID = FirebaseAuthService.getCurrentUser()?.uid

        if (currentUserID == null) {
            onError("Hochladen des Bildes nicht möglich!")
            return
        }


        val fileRef = firebaseStorage.reference.child("Post Pictures")
            .child(currentUserID).child(System.currentTimeMillis().toString() + ".jpg")

        val uploadTask = fileRef.putFile(uri)
        uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
            if (task.isSuccessful)
                return@Continuation fileRef.downloadUrl
            else
                task.exception?.let { throw it }
        }).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val updateInfo = PostDto(
                    description = text,
                    publisher =  currentUserID,
                    postImageURL = task.result.toString()
                )
                FirebasePostService.savePost(updateInfo, onSuccess, onError)
            } else {
                onError("Hochladen des Bildes nicht möglich!")
            }
        }
    }


    fun uploadStoryImage(uri: Uri,onSuccess: () -> Unit, onError: (message: String) -> Unit) {
        val currentUserID = FirebaseAuthService.getCurrentUser()?.uid

        if (currentUserID == null) {
            onError("Hochladen des Bildes nicht möglich!")
            return
        }


        val fileRef = firebaseStorage.reference.child("Story Pictures")
            .child(currentUserID).child(System.currentTimeMillis().toString() + ".jpg")

        val uploadTask = fileRef.putFile(uri)
        uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
            if (task.isSuccessful)
                return@Continuation fileRef.downloadUrl
            else
                task.exception?.let { throw it }
        }).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val storyInfo = StoryDto(
                  imageURL = task.result.toString(),
                  userId = currentUserID
                )
                FirebaseStoryService.saveStory(storyInfo, onSuccess, onError)
            } else {
                onError("Hochladen des Bildes nicht möglich!")
            }
        }
    }
}