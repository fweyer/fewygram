package de.fewycoding.fewygram.network.firebase.auth

import android.util.Log
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import de.fewycoding.fewygram.domain.AuthUserDataDto
import de.fewycoding.fewygram.network.firebase.database.FirebaseUserService

object FirebaseAuthService {

    private val firebaseAuth = FirebaseAuth.getInstance()

    fun registerUser(authUserDataDto: AuthUserDataDto, onSuccess: () -> Unit, onError: (message: String) -> Unit
    ){
        firebaseAuth.createUserWithEmailAndPassword(authUserDataDto.email,authUserDataDto.password)
            .addOnCompleteListener { task ->
                val uuid = firebaseAuth.currentUser?.uid
                if (task.isSuccessful && uuid != null){
                    FirebaseUserService.saveUser(uuid, authUserDataDto, onSuccess, onError)
                }else{
                    Log.e(javaClass.simpleName, task.exception.toString())
                    onError("Registrierung nicht möglich.")
                    firebaseAuth.signOut()
                }
        }
    }

    fun loginUser(email: String, password : String,onSuccess: () -> Unit, onError: (message: String) -> Unit){
        firebaseAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener { task ->
            if(task.isSuccessful){
                onSuccess()
            }
            else{
                Log.e(javaClass.simpleName, task.exception.toString())
                onError("Login nicht möglich. Email oder Passwort nicht korrekt!")
            }
        }
    }

    fun deleteCurrentAccount(passwort: String ,onSuccess: () -> Unit, onError: (String) -> Unit) {
        val currentUser = getCurrentUser()
        val credential = currentUser?.email?.let { EmailAuthProvider.getCredential(it,passwort) };
        if (credential == null){
            onError("Löschen des Accounts nicht möglich")
            return
        }
        currentUser.reauthenticate(credential).addOnCompleteListener { task ->
            if(task.isSuccessful){
                FirebaseUserService.deleteCurrentUser(currentUser.uid,onSuccess,onError)
                logOutCurrentUser()
                currentUser.delete()
            }
            else{
                Log.e(javaClass.simpleName, task.exception.toString())
                onError("Löschen des Accounts nicht möglich")
            }
        }
    }


    fun isUserAlreadyLogIn() : Boolean = firebaseAuth.currentUser != null

    fun logOutCurrentUser() = firebaseAuth.signOut()

    fun getCurrentUser() : FirebaseUser? = firebaseAuth.currentUser
}