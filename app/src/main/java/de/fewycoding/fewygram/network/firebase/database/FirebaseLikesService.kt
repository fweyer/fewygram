package de.fewycoding.fewygram.network.firebase.database

import android.util.Log
import com.google.firebase.database.*
import de.fewycoding.fewygram.network.firebase.auth.FirebaseAuthService

object FirebaseLikesService {

    private val firebaseDatabase = FirebaseDatabase.getInstance()

    fun togglePostLike(postID: String?, onComplition : (isLike : Boolean) -> Unit){
        val currentUserID = FirebaseAuthService.getCurrentUser()?.uid

        if (!isCurrentUsersValid(currentUserID))
            return

        if (!isPostIDValid(postID))
            return

        firebaseDatabase.reference.child("likes").child(postID!!).get().addOnCompleteListener { task ->
            if (task.result.child(currentUserID!!).exists()){
                task.result.child(currentUserID).ref.removeValue()
                onComplition(false)
            } else{
                task.result.child(currentUserID).ref.setValue(true)
                onComplition(true)
            }
        }
    }

    fun isPostLiked(postID: String?, onComplition : (isLike : Boolean) -> Unit){
        val currentUserID = FirebaseAuthService.getCurrentUser()?.uid

        if (!isCurrentUsersValid(currentUserID))
            return

        if (!isPostIDValid(postID))
            return

        firebaseDatabase.reference.child("likes").child(postID!!).get().addOnCompleteListener { task ->
            if (task.result.child(currentUserID!!).exists()){
                onComplition(true)
            } else{
                onComplition(false)
            }
        }
    }

    fun countLikes(postID: String?, onComplition : (size : Int?) -> Unit){
        if (postID == null){
            Log.e(javaClass.simpleName, "postID is null")
            return
        }
        firebaseDatabase.reference.child("likes").child(postID).get().addOnSuccessListener {
            val size = it.children.count()
            onComplition(size)
        }
    }

    fun fetchUserIDs(postID: String, onComplition : (userIDs : List<String>) -> Unit) {
        val userIdList = mutableListOf<String>()
        firebaseDatabase.reference.child("likes").child(postID).get().addOnCompleteListener { task ->
            for (userID in task.result.children){
                val user = userID.key as String
                userIdList.add(user)
            }
            onComplition(userIdList)
        }
    }


    private fun isCurrentUsersValid(currentUserUUID: String?) : Boolean{
        if (currentUserUUID == null){
            Log.e(javaClass.simpleName, "postID is null")
            return false
        }

        return true
    }

    private fun isPostIDValid(postID:  String?) : Boolean{
        if (postID == null){
            Log.e(javaClass.simpleName, "The uuid of the Person is null! Following not possible")
            return false
        }
        return true
    }
}