package de.fewycoding.fewygram.network.firebase.database

import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import de.fewycoding.fewygram.domain.AuthUserDataDto
import de.fewycoding.fewygram.domain.UpdateProfileImageDto
import de.fewycoding.fewygram.domain.UpdateUserDto
import de.fewycoding.fewygram.domain.UserDto
import de.fewycoding.fewygram.network.firebase.auth.FirebaseAuthService
import java.util.*

object FirebaseUserService {

    private val firebaseDatabase = FirebaseDatabase.getInstance()

    fun saveUser(
        uuid: String, authUserDataDto: AuthUserDataDto, onSuccess: () -> Unit, onError: (message: String) -> Unit
    ){
        val userRef = firebaseDatabase.reference.child("users")
        val userMap = HashMap<String,Any>()
        userMap["uuid"] = uuid
        userMap["fullname"] = authUserDataDto.fullName
        userMap["username"] = authUserDataDto.userName
        userMap["email"] = authUserDataDto.email
        userMap["bio"] = authUserDataDto.bio ?: "Hey ich benutze die Fewygram App"
        userMap["imageURL"] = "gs://fewygram.appspot.com/profile.png"

        userRef.child(uuid).setValue(userMap).addOnCompleteListener { task ->
            if (task.isSuccessful)
                onSuccess()
            else
                onError(task.exception.toString())
        }
    }

    fun updateUser(updateInfo: UpdateUserDto, onSuccess: () -> Unit, onError: (message: String) -> Unit){
        val currentUserID = FirebaseAuthService.getCurrentUser()?.uid

        if (!isCurrentUsersValid(currentUserID)){
            onError("Aktualisierung fehlgeschlagen")
            return
        }


        val userRef = firebaseDatabase.reference.child("users")
        val userMap = HashMap<String,Any>()
        userMap["fullname"] = updateInfo.fullName
        userMap["username"] = updateInfo.userName
        userMap["bio"] = updateInfo.bio ?: "Hey ich benutze die Fewygram App"

        userRef.child(currentUserID!!).updateChildren(userMap).addOnCompleteListener { task ->
            if (task.isSuccessful)
                onSuccess()
            else
                onError(task.exception.toString())
        }
    }

    fun updateUserProfileImage(updateInfo: UpdateProfileImageDto, onSuccess: () -> Unit, onError: (message: String) -> Unit){
        val userRef = firebaseDatabase.reference.child("users")
        val userMap = HashMap<String,Any>()
        userMap["imageURL"] = updateInfo.imageUrl

        userRef.child(updateInfo.uuid).updateChildren(userMap).addOnCompleteListener { task ->
            if (task.isSuccessful)
                onSuccess()
            else
                onError(task.exception.toString())
        }
    }

    fun searchUsers(inputText: String?, onComplition: (userDtoList: MutableList<UserDto>) -> Unit)  {
        val userRef = firebaseDatabase.reference.child("users")
        val userList = mutableListOf<UserDto>()

        val query = userRef.orderByChild("fullname")
            .startAt(inputText)
            .endAt(inputText + "\uf8ff")

        query.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                snapshot.children.forEach {
                    val user = it.getValue(UserDto::class.java)
                    if (user != null) userList.add(user)
                }
                onComplition(userList)
            }
            override fun onCancelled(error: DatabaseError) {
                return
            }
        })
    }

    fun fetchCurrentUser(onComplition: (userDto : UserDto?) -> Unit) {
        val currentUserID = FirebaseAuthService.getCurrentUser()?.uid

        if (!isCurrentUsersValid(currentUserID))
            return

        firebaseDatabase.reference.child("users").child(currentUserID!!).get().addOnSuccessListener {
            val user = it.getValue(UserDto::class.java)
            onComplition(user)
        }
    }

    fun fetchUser(uuid : String?, onComplition: (userDto : UserDto?) -> Unit ){
        if (uuid == null)
            return

        firebaseDatabase.reference.child("users").child(uuid).get().addOnSuccessListener {
            val user = it.getValue(UserDto::class.java)
            onComplition(user)
        }
    }

    fun fetchUsersByLikes(postID : String, onComplition: (userDtoList: MutableList<UserDto>) -> Unit){
        val userRef = firebaseDatabase.reference.child("users")
        val userList = mutableListOf<UserDto>()
        FirebaseLikesService.fetchUserIDs(postID){ userIDs ->
               userRef.addValueEventListener(object : ValueEventListener {
                   override fun onDataChange(snapshot: DataSnapshot) {
                       userIDs.forEach { id ->
                          val user = snapshot.child(id).getValue(UserDto::class.java)
                          user?.let { userList.add(user) }
                       }
                       onComplition(userList)
                   }
                   override fun onCancelled(error: DatabaseError) {
                        return
                   }
               })
        }
    }

    fun fetchUsersByFollowers(userID : String, onComplition: (userDtoList: MutableList<UserDto>) -> Unit) {
        val userRef = firebaseDatabase.reference.child("users")
        val userList = mutableListOf<UserDto>()
        FirebaseFollowService.fetchFollowerUserIDs(userID){ userIDs ->
            userRef.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    userIDs.forEach { id ->
                        val user = snapshot.child(id).getValue(UserDto::class.java)
                        user?.let { userList.add(user) }
                    }
                    onComplition(userList)
                }
                override fun onCancelled(error: DatabaseError) {
                    return
                }
            })
        }
    }

    fun fetchUsersByFollowing(userID : String, onComplition: (userDtoList: MutableList<UserDto>) -> Unit) {
        val userRef = firebaseDatabase.reference.child("users")
        val userList = mutableListOf<UserDto>()
        FirebaseFollowService.fetchFollowingUserIDs(userID){ userIDs ->
            userRef.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    userIDs.forEach { id ->
                        val user = snapshot.child(id).getValue(UserDto::class.java)
                        user?.let { userList.add(user) }
                    }
                    onComplition(userList)
                }
                override fun onCancelled(error: DatabaseError) {
                    return
                }
            })
        }
    }

    fun deleteCurrentUser(currentUserID: String, onSuccess: () -> Unit, onError: (String) -> Unit) {
        firebaseDatabase.reference.child("users").child(currentUserID).removeValue().addOnCompleteListener { task ->
            if (task.isSuccessful)
                onSuccess()
            else
                onError("Der User konnte nicht gelöscht werden")
        }
    }

    private fun isCurrentUsersValid(currentUserUUID: String?) : Boolean{
        if (currentUserUUID == null){
            Log.e(javaClass.simpleName, "No User is logt in! Following not possible")
            return false
        }

        return true
    }
}