package de.fewycoding.fewygram.network.firebase.database

import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import de.fewycoding.fewygram.domain.CommentDto
import de.fewycoding.fewygram.domain.PostDto
import de.fewycoding.fewygram.network.firebase.auth.FirebaseAuthService

object FirebaseCommentsService {

    private val firebaseDatabase = FirebaseDatabase.getInstance()

    fun addComment(postDTO: PostDto?, comment: String, onComplition: () -> Unit) {
        if (postDTO == null && postDTO?.postID == null){
            Log.e(javaClass.simpleName, "postID is null")
            return
        }


        val currentUserID = FirebaseAuthService.getCurrentUser()?.uid
        if (!isCurrentUsersValid(currentUserID))
            return

        val commentRef = firebaseDatabase.reference.child("comments").child(postDTO.postID!!)
        val commentsMap = HashMap<String,Any>()
        commentsMap["comment"] = comment
        commentsMap["publisher"] = currentUserID.toString()
        commentRef.push().setValue(commentsMap).addOnSuccessListener {
           onComplition()
        }
    }

    fun countComments(postID: String?, onComplition : (size : Int?) -> Unit){
        if (postID == null){
            Log.e(javaClass.simpleName, "postID is null")
            return
        }
        firebaseDatabase.reference.child("comments").child(postID).get().addOnSuccessListener {
            val size = it.children.count()
            onComplition(size)
        }
    }

    fun fetchComments(postID: String?, onComplition: (List<CommentDto>) -> Unit) {
        if (postID == null){
            Log.e(javaClass.simpleName, "postID is null")
            return
        }

        val commentRef = firebaseDatabase.reference.child("comments").child(postID)
        val commentList = mutableListOf<CommentDto>()
        commentRef.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    snapshot.children.forEach {
                        val comment = it.getValue(CommentDto::class.java)
                        if (comment != null)
                            commentList.add(comment)
                        }
                    onComplition(commentList)
                }
                override fun onCancelled(error: DatabaseError) {
                    return
                }
        })
    }

    private fun isCurrentUsersValid(currentUserUUID: String?) : Boolean{
        if (currentUserUUID == null){
            Log.e(javaClass.simpleName, "postID is null")
            return false
        }

        return true
    }
}