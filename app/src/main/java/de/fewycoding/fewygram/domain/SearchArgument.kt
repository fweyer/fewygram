package de.fewycoding.fewygram.domain

enum class SearchArgument {
    SHOW_LIKES_USERS, SHOW_FOLLOWER_USERS, SHOW_FOLLOWING_USERS
}