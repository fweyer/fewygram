package de.fewycoding.fewygram.domain

data class AuthUserDataDto(
    val fullName : String,
    val userName : String,
    val email: String,
    val password: String,
    val bio: String? = null
)
