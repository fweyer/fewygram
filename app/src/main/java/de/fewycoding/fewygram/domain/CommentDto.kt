package de.fewycoding.fewygram.domain

data class CommentDto(
    val comment : String? = null,
    val publisher : String? = null
)
