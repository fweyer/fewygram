package de.fewycoding.fewygram.domain

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class PostDto(
    var postID: String? = null,
    val description: String? = null,
    val publisher: String? = null,
    val postImageURL: String? = null,
):Parcelable