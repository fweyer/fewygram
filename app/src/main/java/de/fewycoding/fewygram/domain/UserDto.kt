package de.fewycoding.fewygram.domain

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class UserDto(
    val uuid : String? = null,
    val fullname : String? = null,
    val username : String? = null,
    val email: String? = null,
    val bio: String? = null,
    val imageURL: String? = null,
) : Parcelable
