package de.fewycoding.fewygram.domain

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class StoryDto(
    var storyId : String? = null,
    var userId : String? = null,
    var imageURL : String? = null,
    var timeEnd : Long? = null
) : Parcelable
