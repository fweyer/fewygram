package de.fewycoding.fewygram.domain

enum class DurrationEnum {
    ONE_MINUTE, ONE_HOUR, ONE_DAY
}