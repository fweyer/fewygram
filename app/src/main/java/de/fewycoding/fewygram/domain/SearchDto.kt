package de.fewycoding.fewygram.domain

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class SearchDto(
    val searchArgument : SearchArgument,
    val searchID : String
): Parcelable
