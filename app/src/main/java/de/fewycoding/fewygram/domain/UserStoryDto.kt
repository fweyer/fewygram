package de.fewycoding.fewygram.domain

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class UserStoryDto(
    val userId: String? = null,
    val storyDtoList: List<StoryDto>? = null
) : Parcelable