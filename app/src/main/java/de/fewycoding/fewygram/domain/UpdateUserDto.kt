package de.fewycoding.fewygram.domain

data class UpdateUserDto(
    val fullName : String,
    val userName : String,
    val bio: String? = null
)
