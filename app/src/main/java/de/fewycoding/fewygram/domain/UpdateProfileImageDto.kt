package de.fewycoding.fewygram.domain

data class UpdateProfileImageDto(
    val uuid: String,
    val imageUrl: String
)
