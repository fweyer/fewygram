package de.fewycoding.fewygram.screen.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import de.fewycoding.fewygram.R
import de.fewycoding.fewygram.domain.AuthUserDataDto
import de.fewycoding.fewygram.network.firebase.auth.FirebaseAuthService
import de.fewycoding.fewygram.screen.dialog.DialogFactory
import de.fewycoding.fewygram.screen.dialog.components.ProgressDialog

class RegisterActivity : AppCompatActivity() {

    private lateinit var  progressDialog : ProgressDialog
    private lateinit var  fullnameEditText: EditText
    private lateinit var  userEditText: EditText
    private lateinit var  emailEditText: EditText
    private lateinit var  passwortEditText: EditText
    private lateinit var  registerBtn : Button
    private lateinit var  backToLogin : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        initialize()
        initProgressDialog()
    }

    private fun initialize(){
        fullnameEditText = findViewById(R.id.registeractivity_edittext_fullname)
        userEditText = findViewById(R.id.registeractivity_edittext_username)
        emailEditText = findViewById(R.id.registeractivity_edittext_email)
        passwortEditText = findViewById(R.id.registeractivity_edittext_passwort)
        registerBtn = findViewById(R.id.registeractivity_btn_register)
        backToLogin = findViewById(R.id.registeractivity_btn_backToLogin)

        backToLogin.setOnClickListener { navigateToLoginActivity() }
        registerBtn.setOnClickListener { createAccount() }
    }

    private fun initProgressDialog(){
        val onBackPressedAction = { finish() }
        progressDialog = DialogFactory.createProgrssDialog(this,  "Registrierung", "Bitte Warten...", onBackPressedAction)
    }

    private fun navigateToLoginActivity(){
        startActivity(Intent(this, LoginActivity::class.java))
    }

    private fun createAccount(){
        val fullName = fullnameEditText.text.toString()
        val userName = userEditText.text.toString()
        val email = emailEditText.text.toString()
        val password = passwortEditText.text.toString()

        when {
            TextUtils.isEmpty(fullName) -> Toast.makeText(this, "Bitte geben Sie den Fullname ein",Toast.LENGTH_LONG).show()
            TextUtils.isEmpty(userName) -> Toast.makeText(this, "Bitte geben Sie den Username ein",Toast.LENGTH_LONG).show()
            TextUtils.isEmpty(email) -> Toast.makeText(this, "Bitte geben Sie die Email ein",Toast.LENGTH_LONG).show()
            TextUtils.isEmpty(password) -> Toast.makeText(this, "Bitte geben Sie das Passwort ein",Toast.LENGTH_LONG).show()
            else -> {
                progressDialog.show(600,600)
                registerUser(AuthUserDataDto(fullName, userName, email, password))
            }
        }
    }

    private fun registerUser(authUserDataDto: AuthUserDataDto) {
        val onSuccess =  { navigateToMainActivity() }
        val onError =  { it: String -> showErrorToast(it)}
        FirebaseAuthService.registerUser(authUserDataDto,onSuccess,onError)
    }

    private fun navigateToMainActivity(){
        progressDialog.dismiss()
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    private fun showErrorToast(message: String){
        Toast.makeText(this, message,Toast.LENGTH_LONG).show()
        progressDialog.dismiss()
    }

}