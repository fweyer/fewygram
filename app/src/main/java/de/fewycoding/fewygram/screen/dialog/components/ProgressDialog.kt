package de.fewycoding.fewygram.screen.dialog.components

import android.app.AlertDialog
import android.content.Context
import android.view.KeyEvent
import android.view.LayoutInflater
import android.widget.TextView
import de.fewycoding.fewygram.R

class ProgressDialog(
    context: Context,
    title: String,
    message: String,
    onBackPressAction: () -> Unit
) {

  private var dialog : AlertDialog

    init {
        val builder = AlertDialog.Builder(context)
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_progressbar, null, false)
        view.findViewById<TextView>(R.id.dialog_progressbar_title).text = title
        view.findViewById<TextView>(R.id.dialog_progressbar_message).text = message
        builder.setCancelable(false)
        builder.setOnKeyListener { _, keycode, _ ->
            if (keycode == KeyEvent.KEYCODE_BACK){
                dismiss()
                onBackPressAction()
            }
            false
        }
        builder.setView(view)
        dialog = builder.create()
    }

   fun show(width : Int, height : Int){
       dialog.show()
       dialog.window?.setLayout(width,height)
   }

   fun dismiss(){
       dialog.dismiss()
   }

}