package de.fewycoding.fewygram.screen.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import de.fewycoding.fewygram.screen.activity.AccountSettingsActivity
import de.fewycoding.fewygram.R
import de.fewycoding.fewygram.domain.PostDto
import de.fewycoding.fewygram.domain.SearchArgument
import de.fewycoding.fewygram.domain.SearchDto
import de.fewycoding.fewygram.domain.UserDto
import de.fewycoding.fewygram.network.firebase.database.FirebaseFollowService
import de.fewycoding.fewygram.network.firebase.database.FirebasePostService
import de.fewycoding.fewygram.network.firebase.database.FirebaseUserService
import de.fewycoding.fewygram.screen.activity.ShowUsersActivity
import de.fewycoding.fewygram.screen.adapter.CurrentUserImagesAdapter
import de.fewycoding.fewygram.screen.dialog.DialogFactory
import de.fewycoding.fewygram.screen.dialog.components.ShowImageDialog
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.coroutines.*


@DelicateCoroutinesApi
class ProfileFragment : Fragment() {

   private lateinit var container : RelativeLayout
   private lateinit var llFollowers : LinearLayout
   private lateinit var llFollowing : LinearLayout
   private lateinit var progressBar: ProgressBar
   private lateinit var profileImageView : CircleImageView
   private lateinit var textViewUsername: TextView
   private lateinit var textViewFullname: TextView
   private lateinit var textViewBio: TextView
   private lateinit var textViewFollowers : TextView
   private lateinit var textViewFollowing : TextView
   private lateinit var textViewPosts : TextView
   private lateinit var editProfileButton : Button
   private lateinit var recyclerViewUploaded: RecyclerView
   private lateinit var showDialogImage : ShowImageDialog

   private var currentUserDto : UserDto? = null
   private var postList : MutableList<PostDto>? = null
   private var currentImageAdapter: CurrentUserImagesAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, viewGroup: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_profile, viewGroup, false)
        initializeView(view)
        progressBar.visibility = View.VISIBLE
        container.visibility = View.GONE
        configRecyclerView()
        fetchCurrentUserInfos()
        return view
    }

    private fun initializeView(view: View){
        container = view.findViewById(R.id.profile_fragment_container)
        progressBar = view.findViewById(R.id.profile_fragment_progressbar)
        textViewUsername = view.findViewById(R.id.profile_fragment_username)
        textViewFullname = view.findViewById(R.id.profile_fragment_fullname)
        textViewBio = view.findViewById(R.id.profile_fragment_bio)
        textViewFollowers = view.findViewById(R.id.profile_fragment_total_followers)
        textViewFollowing = view.findViewById(R.id.profile_fragment_total_following)
        textViewPosts = view.findViewById(R.id.profile_fragment_total_posts)
        profileImageView = view.findViewById(R.id.profile_fragment_circleimage_view)
        editProfileButton = view.findViewById(R.id.profile_fragment_btn_editprofile)
        recyclerViewUploaded = view.findViewById(R.id.profile_fragment_recyclerView_uploaded)
        editProfileButton.setOnClickListener { navigateToAccountSettings() }
        llFollowers = view.findViewById(R.id.profile_fragment_followers_linearLayout)
        llFollowing = view.findViewById(R.id.profile_fragment_following_linearLayout)
        llFollowers.setOnClickListener { navigateToShowUsersActivityByFollowers() }
        llFollowing.setOnClickListener { navigateToShowUsersActivityByFollowing() }
    }

    private fun configRecyclerView(){
        val gridLayoutManager = GridLayoutManager(context,3)
        gridLayoutManager.orientation = LinearLayoutManager.VERTICAL
        postList = ArrayList()
        recyclerViewUploaded.layoutManager = gridLayoutManager
        currentImageAdapter = context?.let { CurrentUserImagesAdapter(it,postList as ArrayList<PostDto>) }
        recyclerViewUploaded.adapter = currentImageAdapter
    }

    fun fetchCurrentImages() {
        FirebasePostService.fetchPostFromCurrentUser { postList ->
            updateView(postList)
        }
    }

    private fun fetchCurrentUserInfos(){
        val setCurrentUserInfoAndFetchFollowInfos = { userDto : UserDto? ->
            currentUserDto = userDto
            setCurrentUserInfos(currentUserDto)
            fetchFollowInformation(currentUserDto)
            fetchCurrentImages()
        }
        FirebaseUserService.fetchCurrentUser(setCurrentUserInfoAndFetchFollowInfos)
    }

    private fun setCurrentUserInfos(currentUserDto: UserDto?) {
        currentUserDto?.let {
            textViewUsername.text = currentUserDto.username
            textViewFullname.text = currentUserDto.fullname
            textViewBio.text = currentUserDto.bio
            configProfileImageView(it, profileImageView)
        }
    }

    private fun fetchFollowInformation(currentUserDto: UserDto?) {
        currentUserDto?.uuid?.let {
            GlobalScope.launch  (Dispatchers.IO) {
                launch { fetchFollowerSizeCurrentUser(it) }
                launch { fetchFollowingSizeCurrentUser(it) }
            }
        }
    }

    private  fun fetchFollowingSizeCurrentUser(userId : String){
        val setFollowing =  { size : Int? -> textViewFollowing.text = size.toString() }
        FirebaseFollowService.fetchFollowingSize(userId,setFollowing)
    }

    private fun fetchFollowerSizeCurrentUser(userId : String){
        val setFollowers =  { size : Int? -> textViewFollowers.text = size.toString() }
        FirebaseFollowService.fetchFollowerSize(userId,setFollowers)
    }

    private fun configProfileImageView(userDto: UserDto, imageView: CircleImageView){
        userDto.imageURL?.let { imageURL ->
            context?.let { showDialogImage = DialogFactory.createShowImageDialog(it,imageURL) }
        }
        imageView.setOnClickListener { showDialogImage.show() }
        Picasso.get().load(userDto.imageURL).into(imageView)
    }

    private fun navigateToAccountSettings(){
        val bundle = Bundle()
        bundle.putParcelable(AccountSettingsActivity.USER_ID, currentUserDto)
        val intent = Intent(context, AccountSettingsActivity::class.java)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun updateView(posts: List<PostDto>) {
        postList?.clear()
        postList?.addAll(posts)
        textViewPosts.text = postList?.size.toString()
        currentImageAdapter?.notifyDataSetChanged()
        progressBar.visibility = View.GONE
        container.visibility = View.VISIBLE
    }

    private fun navigateToShowUsersActivityByFollowers(){
        currentUserDto?.uuid?.let {
            val bundle = Bundle()
            val searchDTO = SearchDto(SearchArgument.SHOW_FOLLOWER_USERS, it)
            bundle.putParcelable(ShowUsersActivity.SEARCH_DTO, searchDTO)
            val intent = Intent(context, ShowUsersActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent,bundle)
        }
    }

    private fun navigateToShowUsersActivityByFollowing(){
        currentUserDto?.uuid?.let {
            val bundle = Bundle()
            val searchDTO = SearchDto(SearchArgument.SHOW_FOLLOWING_USERS, it)
            bundle.putParcelable(ShowUsersActivity.SEARCH_DTO, searchDTO)
            val intent = Intent(context, ShowUsersActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent,bundle)
        }
    }
}