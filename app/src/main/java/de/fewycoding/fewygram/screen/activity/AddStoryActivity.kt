package de.fewycoding.fewygram.screen.activity

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import de.fewycoding.fewygram.R
import de.fewycoding.fewygram.network.firebase.storage.FirebaseStorageService
import de.fewycoding.fewygram.screen.dialog.DialogFactory
import de.fewycoding.fewygram.screen.dialog.components.ProgressDialog

class AddStoryActivity : AppCompatActivity() {

    private var imageURI : Uri? = null
    private lateinit var progressDialog : ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_story)
        initProgressDialog()
        cropImage()
    }

    private fun initProgressDialog(){
        val onBackPressedAction = { finish() }
        progressDialog = DialogFactory.createProgrssDialog(
            this, "Story wird hochgeladen", "Bitte Warten...", onBackPressedAction
        )
    }

    private fun cropImage(){
        CropImage.activity()
            .setAspectRatio(1,1)
            .setGuidelines(CropImageView.Guidelines.ON)
            .start(this@AddStoryActivity)
    }


    private fun uploadStory() {
        if (!isInputValid()){
            Toast.makeText(this, "Bitte ein Bild auswählen", Toast.LENGTH_LONG).show()
            return
        }
        progressDialog.show(800,600)
        imageURI?.let {
            val onSuccess = { succesfulUpdate() }
            val onError = { message: String -> showErrorToast(message) }
            FirebaseStorageService.uploadStoryImage(imageURI!!,onSuccess,onError)
        }

    }

    private fun succesfulUpdate(){
        Toast.makeText(this, "Story erfolgreich hochgeladen",Toast.LENGTH_LONG).show()
        progressDialog.dismiss()
        finish()
    }

    private fun showErrorToast(message : String){
        Toast.makeText(this, message,Toast.LENGTH_LONG).show()
        progressDialog.dismiss()
        finish()
    }

    private fun isInputValid() : Boolean{
        return when (imageURI) {
            null -> false
            else -> true
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE &&
            resultCode == Activity.RESULT_OK &&
            data != null)
        {
            val result = CropImage.getActivityResult(data)
            imageURI = result.uri
            uploadStory()
        }
        else
        {
            finish()
        }
    }
}