package de.fewycoding.fewygram.screen.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import de.fewycoding.fewygram.R
import de.fewycoding.fewygram.screen.fragment.HomeFragment
import de.fewycoding.fewygram.screen.fragment.ProfileFragment
import de.fewycoding.fewygram.screen.fragment.SearchFragment

class MainActivity : AppCompatActivity() {

    private lateinit var navView : BottomNavigationView
    private val homeFragment: HomeFragment = HomeFragment()
    private val searchFragment: SearchFragment = SearchFragment()
    private val profileFragment: ProfileFragment = ProfileFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        navView = findViewById(R.id.nav_view)
        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
        moveToFragment(HomeFragment())
    }

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.nav_home -> {
                moveToFragment(homeFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_search -> {
                moveToFragment(searchFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_add_post -> {
                item.isChecked = false
                startActivity(Intent(this, AddPostActivity::class.java))
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_profile -> {
                moveToFragment(profileFragment)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun moveToFragment(fragment: Fragment){
        val fragmentTrans = supportFragmentManager.beginTransaction()
        fragmentTrans.replace(R.id.container, fragment)
        fragmentTrans.commit()
    }

}