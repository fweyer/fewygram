package de.fewycoding.fewygram.screen.activity

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import de.fewycoding.fewygram.R
import de.fewycoding.fewygram.domain.PostDto
import de.fewycoding.fewygram.domain.SearchArgument
import de.fewycoding.fewygram.domain.SearchDto
import de.fewycoding.fewygram.domain.UserDto
import de.fewycoding.fewygram.network.firebase.database.FirebaseFollowService
import de.fewycoding.fewygram.network.firebase.database.FirebasePostService
import de.fewycoding.fewygram.screen.adapter.CurrentUserImagesAdapter
import de.fewycoding.fewygram.screen.dialog.DialogFactory
import de.fewycoding.fewygram.screen.dialog.components.ShowImageDialog
import de.hdodenhof.circleimageview.CircleImageView

class UserProfileActivity : AppCompatActivity() {

    companion object {
        const val USER_ID: String = "USER_ID"
    }
    private lateinit var llFollowers : LinearLayout
    private lateinit var llFollowing : LinearLayout
    private lateinit var textFollowers: TextView
    private lateinit var textFollowing: TextView
    private lateinit var textViewPosts: TextView
    private lateinit var circleImageView: CircleImageView
    private lateinit var progressBarFollowers: ProgressBar
    private lateinit var progressBarFollowing: ProgressBar
    private lateinit var followButton: Button
    private lateinit var recyclerView : RecyclerView
    private lateinit var showDialogImage : ShowImageDialog

    private var userDto : UserDto? = null
    private var postList : MutableList<PostDto>? = null
    private var currentImageAdapter: CurrentUserImagesAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_profile)
        userDto = intent.extras?.getParcelable(USER_ID)
        userDto?.let {
            initialize(it)
            configProfileImageView(it)
            configFollowButton(it)
            fetchFollowInformation(it.uuid)
            configRecyclerView()
            fetchCurrentImages()
        }
    }

    private fun initialize(userDto: UserDto) {
        textFollowers = findViewById(R.id.activity_user_profile_total_followers)
        textFollowing = findViewById(R.id.activity_user_profile_total_following)
        llFollowers = findViewById(R.id.activity_user_profile_followers_linearlayout)
        llFollowing = findViewById(R.id.activity_user_profile_following_linearlayout)
        llFollowers.setOnClickListener { navigateToShowUsersActivityByFollowers() }
        llFollowing.setOnClickListener { navigateToShowUsersActivityByFollowing() }
        textViewPosts = findViewById(R.id.activity_user_profile_total_posts)
        circleImageView = findViewById(R.id.activity_user_profile_circleimage_view)
        followButton = findViewById(R.id.activity_user_profile_btn_follow)
        progressBarFollowers = findViewById(R.id.activity_user_profile_progressbar_followers)
        progressBarFollowing = findViewById(R.id.activity_user_profile_progressbar_following)
        recyclerView = findViewById(R.id.activity_user_profile_recyclerView)
        findViewById<TextView>(R.id.activity_user_profile_username).text = userDto.username
        findViewById<TextView>(R.id.activity_user_profile_fullname).text = userDto.fullname
        findViewById<TextView>(R.id.activity_user_profile_bio).text = userDto.bio
    }

    private fun configRecyclerView(){
        val gridLayoutManager = GridLayoutManager(this,3)
        gridLayoutManager.orientation = LinearLayoutManager.VERTICAL
        postList = ArrayList()
        recyclerView.layoutManager = gridLayoutManager
        recyclerView.adapter = CurrentUserImagesAdapter(this,postList as ArrayList<PostDto>)
    }

    private fun fetchCurrentImages() {
        userDto?.uuid?.let {
            FirebasePostService.fetchPostFromUser(it) { postList ->
                updateRecylerViewUploaded(postList)
            }
        }
    }

    private fun fetchFollowInformation(uuid: String?) {
        uuid?.let {
            fetchFollowerSizeCurrentUser(uuid)
            fetchFollowingSizeCurrentUser(uuid)
        }
    }

    private fun fetchFollowingSizeCurrentUser(uuid: String) {
        val onComplition = { size: Int? -> showFollowingResults(size) }
        FirebaseFollowService.fetchFollowingSize(uuid, onComplition)
    }

    private fun fetchFollowerSizeCurrentUser(uuid: String) {
        val onComplition = { size: Int? -> showFollowerResults(size) }
        FirebaseFollowService.fetchFollowerSize(uuid, onComplition)
    }

    private fun showFollowingResults(size: Int?) {
        textFollowing.text = size.toString()
        progressBarFollowing.visibility = View.GONE
        textFollowing.visibility = View.VISIBLE
    }

    private fun showFollowerResults(size: Int?) {
        textFollowers.text = size.toString()
        progressBarFollowers.visibility = View.GONE
        textFollowers.visibility = View.VISIBLE
    }

    @SuppressLint("SetTextI18n")
    private fun configFollowButton(userDto: UserDto) {
        val setButtonTextFolgen = { followButton.text = "Folgen" }
        val setButtonTextNichtFolgen = { followButton.text = "Nicht Folgen" }

        FirebaseFollowService.checkFollingStatus(
            uuidToFollow = userDto.uuid,
            isFollowing = setButtonTextNichtFolgen,
            isNotFollowing = setButtonTextFolgen
        )

        followButton.setOnClickListener {
            val isFollowing = followButton.text == "Folgen"
            val onComplition = { fetchFollowInformation(userDto.uuid) }
            FirebaseFollowService.followOrUnfollowUser(userDto.uuid, isFollowing, onComplition)
            updateButtonText(followButton)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun updateButtonText(btn: Button) {
        if (btn.text == "Folgen") btn.text = "Nicht Folgen"
        else btn.text = "Folgen"
    }


    private fun configProfileImageView(userDto: UserDto) {
        userDto.imageURL?.let { imageURL ->
            showDialogImage = DialogFactory.createShowImageDialog(this,imageURL)
            circleImageView.setOnClickListener { showDialogImage.show() }
            Picasso.get().load(userDto.imageURL).into(circleImageView)
        }
    }


    private fun navigateToShowUsersActivityByFollowers(){
        userDto?.uuid?.let {
            val bundle = Bundle()
            val searchDTO = SearchDto(SearchArgument.SHOW_FOLLOWER_USERS, it)
            bundle.putParcelable(ShowUsersActivity.SEARCH_DTO, searchDTO)
            val intent = Intent(this, ShowUsersActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent,bundle)
            finish()
        }
    }

    private fun navigateToShowUsersActivityByFollowing(){
        userDto?.uuid?.let {
            val bundle = Bundle()
            val searchDTO = SearchDto(SearchArgument.SHOW_FOLLOWING_USERS, it)
            bundle.putParcelable(ShowUsersActivity.SEARCH_DTO, searchDTO)
            val intent = Intent(this, ShowUsersActivity::class.java)
            intent.putExtras(bundle)
            finish()
            startActivity(intent,bundle)
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun updateRecylerViewUploaded(posts: List<PostDto>) {
        postList?.clear()
        postList?.addAll(posts)
        textViewPosts.text = postList?.size.toString()
        currentImageAdapter?.notifyDataSetChanged()
    }
}