package de.fewycoding.fewygram.screen.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import de.fewycoding.fewygram.R
import de.fewycoding.fewygram.domain.PostDto
import de.fewycoding.fewygram.network.firebase.database.FirebasePostService
import de.fewycoding.fewygram.screen.activity.MainActivity
import de.fewycoding.fewygram.screen.activity.PostDetailActivity
import de.fewycoding.fewygram.screen.dialog.DialogFactory
import de.fewycoding.fewygram.screen.fragment.ProfileFragment
import kotlinx.coroutines.DelicateCoroutinesApi

class CurrentUserImagesAdapter(
    private var mContext : Context,
    private var mPostList : MutableList<PostDto>,
) : RecyclerView.Adapter<CurrentUserImagesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.currentuserimages_item,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Picasso.get().load(mPostList[position].postImageURL).into(holder.imageView)
        configViewClick(holder,mPostList[position])
        configViewOnLongClick(holder,mPostList[position])
    }

    override fun getItemCount(): Int {
       return mPostList.size
    }

    private fun configViewClick(holder: ViewHolder, post: PostDto){
        holder.imageView.setOnClickListener {
            navigateToPostDetailsActivity(post)
        }
    }

    private fun configViewOnLongClick(holder: ViewHolder, post: PostDto){
        holder.imageView.setOnLongClickListener {
            if (mContext.javaClass == MainActivity::class.java){
                onLongPressInProfileFragment(post)
                true
            } else {
                false
            }
        }
    }

    @DelicateCoroutinesApi
    private fun onLongPressInProfileFragment(post: PostDto){
        val onPositivButton = {
            FirebasePostService.deletePost(post) {
                mPostList.remove(post)
                val mainActivity = mContext as MainActivity
                val fragment = mainActivity.supportFragmentManager.fragments.last()
                if (fragment.javaClass == ProfileFragment::class.java){
                    ((fragment) as ProfileFragment).fetchCurrentImages()
                }
            }
        }
        DialogFactory.createPromtDialog(
            mContext,"Soll dieses Bild gelöscht werden",onPositivButton, null).show()
    }


    private fun navigateToPostDetailsActivity(post: PostDto?){
        val intent = Intent(mContext, PostDetailActivity::class.java)
        intent.putExtra(PostDetailActivity.POST_DTO,post)
        mContext.startActivity(intent)
    }

    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        val imageView: ImageView = itemView.findViewById(R.id.currentuserimages_imageview)
    }
}