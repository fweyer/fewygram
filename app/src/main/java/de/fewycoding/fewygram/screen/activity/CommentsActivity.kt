package de.fewycoding.fewygram.screen.activity

import android.annotation.SuppressLint
import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import de.fewycoding.fewygram.R
import de.fewycoding.fewygram.domain.CommentDto
import de.fewycoding.fewygram.domain.PostDto
import de.fewycoding.fewygram.domain.UserDto
import de.fewycoding.fewygram.network.firebase.database.FirebaseCommentsService
import de.fewycoding.fewygram.network.firebase.database.FirebaseUserService
import de.fewycoding.fewygram.screen.adapter.CommentAdapter
import de.hdodenhof.circleimageview.CircleImageView


class CommentsActivity : AppCompatActivity() {

    companion object{
        const val POST_DTO : String = "POST_DTO"
    }

    private lateinit var recyclerView: RecyclerView
    private lateinit var commentAdapter: CommentAdapter
    private lateinit var imageViewProfile : CircleImageView
    private lateinit var imageViewPost : ImageView
    private lateinit var editTextComment: EditText
    private lateinit var commentButton : Button
    private lateinit var commentsList : MutableList<CommentDto>

    private var postDTO : PostDto? = null
    private var currentUserDto : UserDto? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comments)
        postDTO = intent.getParcelableExtra(POST_DTO)
        initialize()
        configRecyclerView()
        setPostInformation()
        fetchCurrentUserInfos()
        retriveComments()
    }

    private fun initialize(){
        imageViewProfile = findViewById(R.id.commentsactivity_profile_image)
        imageViewPost = findViewById(R.id.commentsactivity_image_post)
        editTextComment = findViewById(R.id.commentsactivity_editText)
        commentButton = findViewById(R.id.commentsactivity_button)
        recyclerView = findViewById(R.id.commentsactivity_recyclerview)
        commentButton.setOnClickListener { addComment() }
    }

    private fun configRecyclerView(){
        val linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager
        commentsList = ArrayList()
        commentAdapter = CommentAdapter(this, commentsList)
        recyclerView.adapter = commentAdapter
    }


    private fun addComment(){
        val comment = editTextComment.text.toString()
        when {
            TextUtils.isEmpty(comment) -> {}
            else -> {
                val onComplition = { retriveComments() }
                FirebaseCommentsService.addComment(postDTO, comment,onComplition)
            }
        }
    }

    private fun fetchCurrentUserInfos(){
        val setCurrentUserInfo = { userDto : UserDto? ->
            currentUserDto = userDto
            setCurrentUserImage(currentUserDto)
        }
        FirebaseUserService.fetchCurrentUser(setCurrentUserInfo)
    }

    private fun setCurrentUserImage(currentUserDto: UserDto?) {
        currentUserDto?.let {
            Picasso.get()
                .load(currentUserDto.imageURL)
                .placeholder(R.drawable.profile)
                .error(R.drawable.profile)
                .into(imageViewProfile)
        }
    }

    private fun setPostInformation(){
        postDTO?.apply {
            Picasso.get()
                .load(postImageURL)
                .placeholder(R.drawable.profile)
                .error(R.drawable.profile)
                .into(imageViewPost)
        }
    }

    private fun retriveComments(){
        val onComplition = { commentDtos : List<CommentDto>  -> updateViews(commentDtos)}
        FirebaseCommentsService.fetchComments(postDTO?.postID,onComplition)
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun updateViews(commentDtos : List<CommentDto>){
        hideKeyboard(this)
        editTextComment.text.clear()
        commentsList.clear()
        commentsList.addAll(commentDtos)
        commentAdapter?.notifyDataSetChanged()
    }

    private fun hideKeyboard(activity: Activity){
        val imm = activity.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        var view: View? = activity.currentFocus
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}