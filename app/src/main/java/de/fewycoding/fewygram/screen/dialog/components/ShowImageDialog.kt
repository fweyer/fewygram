package de.fewycoding.fewygram.screen.dialog.components

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.widget.ImageView
import com.squareup.picasso.Picasso
import de.fewycoding.fewygram.R

class ShowImageDialog(context: Context, imageURL: String) {

    private var dialog : AlertDialog

    init {
        val builder = AlertDialog.Builder(context)
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_showimage, null, false)
        val imageView = view.findViewById<ImageView>(R.id.dialog_showimage_imageview)
        Picasso.get().load(imageURL).into(imageView)
        builder.setCancelable(true)
        builder.setView(view)
        dialog = builder.create()
    }

    fun show(){
        dialog.show()
    }

}