package de.fewycoding.fewygram.screen.activity

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import com.squareup.picasso.Picasso
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import de.fewycoding.fewygram.R
import de.fewycoding.fewygram.domain.UpdateUserDto
import de.fewycoding.fewygram.domain.UserDto
import de.fewycoding.fewygram.network.firebase.auth.FirebaseAuthService
import de.fewycoding.fewygram.network.firebase.database.FirebaseUserService
import de.fewycoding.fewygram.network.firebase.storage.FirebaseStorageService
import de.fewycoding.fewygram.screen.dialog.DialogFactory
import de.fewycoding.fewygram.screen.dialog.components.ProgressDialog
import de.hdodenhof.circleimageview.CircleImageView

class AccountSettingsActivity : AppCompatActivity() {
    companion object{
        const val USER_ID : String = "USER_ID"
    }

   private lateinit var progressDialog: ProgressDialog
   private lateinit var loginOutbtn : Button
   private lateinit var deleteAccountbtn : Button
   private lateinit var saveButton : ImageView
   private lateinit var cancelButton: ImageView
   private lateinit var imageProfile : CircleImageView
   private lateinit var textViewUsername : EditText
   private lateinit var textViewFullname : EditText
   private lateinit var textViewBio : EditText

   private var currentUserDto : UserDto? = null
   private var isImageSelected : Boolean = false
   private var imageURI : Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_settings)
        currentUserDto = intent.extras?.getParcelable(USER_ID)
        initialize()
        initProgressDialog()
        initCurrentUserInformation()
    }

    private fun initialize(){
        loginOutbtn = findViewById(R.id.accountactivity_button_logout)
        deleteAccountbtn = findViewById(R.id.accountactivity_button_deleteAccount)
        saveButton = findViewById(R.id.accountactivity_save_profile_btn)
        cancelButton = findViewById(R.id.accountactivity_close_profile_btn)
        textViewUsername = findViewById(R.id.accountactivity_edittext_username)
        textViewFullname = findViewById(R.id.accountactivity_edittext_profilename)
        textViewBio = findViewById(R.id.accountactivity_edittext_bio)
        imageProfile = findViewById(R.id.accountactivity_profile_imageview)
        loginOutbtn.setOnClickListener { logOut() }
        deleteAccountbtn.setOnClickListener { createDialogDeleteAccount() }
        saveButton.setOnClickListener { saveUserData() }
        cancelButton.setOnClickListener { finish() }
        imageProfile.setOnClickListener { cropImage() }
    }

    private fun initProgressDialog(){
        val onBackPressedAction = { finish() }
        progressDialog = DialogFactory.createProgrssDialog(
            this, "Aktualisierung läuft", "Bitte Warten...", onBackPressedAction
        )
    }


    private fun initCurrentUserInformation(){
        currentUserDto?.let {
            textViewFullname.setText(currentUserDto?.fullname)
            textViewUsername.setText(currentUserDto?.username)
            textViewBio.setText(currentUserDto?.bio)
            setImageView(it)
        }
    }


    private fun saveUserData(){
        hideKeyboard(this)
        if (!isInputValid()){
            Toast.makeText(this,"Felder dürfen nicht leer sein", Toast.LENGTH_LONG).show()
            progressDialog.dismiss()
            return
        }

        currentUserDto?.let {
            val update = createUpdateUserInformation()
            val onError = { message: String -> showErrorToast(message) }
            if (isImageSelected){
                progressDialog.show(800,600)
                val onSuccess = { uploadImage() }
                FirebaseUserService.updateUser(update,onSuccess, onError)
            }else{
                val onSuccess = { finishActivity() }
                FirebaseUserService.updateUser(update,onSuccess, onError)
            }
        }
    }

    private fun isInputValid() : Boolean{
        return when {
            TextUtils.isEmpty(textViewUsername.text) ->  false
            TextUtils.isEmpty(textViewFullname.text) ->  false
            TextUtils.isEmpty(textViewBio.text) ->  false
            else ->  true
        }
    }

    private fun logOut(){
        FirebaseAuthService.logOutCurrentUser()
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    private fun finishActivity(){
        progressDialog.dismiss()
        finish()
    }

    private fun showErrorToast(message : String){
        Toast.makeText(this, message,Toast.LENGTH_LONG).show()
    }

    private fun hideKeyboard(activity: Activity){
        val imm = activity.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        var view: View? = activity.currentFocus
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private fun createUpdateUserInformation() : UpdateUserDto {
        return UpdateUserDto(
            fullName = textViewFullname.text.toString(),
            userName = textViewUsername.text.toString(),
            bio = textViewBio.text.toString()
        )
    }

    private fun cropImage(){
        CropImage.activity()
            .setAspectRatio(1,1)
            .setGuidelines(CropImageView.Guidelines.ON)
            .start(this@AccountSettingsActivity)
    }

    private fun uploadImage(){
        imageURI?.let {
            val onSuccess = { finishActivity() }
            val onError = { message: String -> showErrorToast(message) }
            FirebaseStorageService.uploadProfileImage(it,onSuccess, onError)
        }
    }

    private fun setImageView(userDto: UserDto?){
        Picasso.get()
            .load(userDto?.imageURL)
            .placeholder(R.drawable.profile)
            .error(R.drawable.profile)
            .into(imageProfile)
    }

    private fun createDialogDeleteAccount(){
        val onPositivButtonClick = { inputText: String -> deleteAccount(inputText) }
        DialogFactory.createPromtDialogDeleteUser(this,onPositivButtonClick,null).show()
    }


    private fun deleteAccount(passwort: String) {
        val onSuccess = { accountSuccessfulDeletedHandling() }
        val onError = { message: String -> accountDeletedErrorHandling(message) }
        FirebaseAuthService.deleteCurrentAccount(passwort,onSuccess,onError)
    }

    private fun accountSuccessfulDeletedHandling(){
        Toast.makeText(this,"Account erfolgreich gelöscht",Toast.LENGTH_LONG).show()
        finish()
        startActivity(Intent(this, LoginActivity::class.java))
    }

    private fun accountDeletedErrorHandling(message: String){
        Toast.makeText(this,message,Toast.LENGTH_LONG).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE &&
           resultCode == Activity.RESULT_OK &&
           data != null)
        {
            val result = CropImage.getActivityResult(data)
            imageURI = result.uri
            imageProfile.setImageURI(imageURI)
            isImageSelected = true
        }
        else
        {
            Toast.makeText(this,"Auswahl des Bildes nicht möglich", Toast.LENGTH_LONG).show()
        }
    }
}