package de.fewycoding.fewygram.screen.activity

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.fewycoding.fewygram.R
import de.fewycoding.fewygram.domain.PostDto
import de.fewycoding.fewygram.screen.adapter.PostAdapter

class PostDetailActivity : AppCompatActivity() {

    companion object {
        const val POST_DTO = "POST_DTO"
    }

    private lateinit var recyclerView: RecyclerView

    private lateinit var postList : MutableList<PostDto>
    private var postAdapter: PostAdapter? = null
    private var postDTO : PostDto? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_detail)
        postDTO = intent.getParcelableExtra(POST_DTO)
        initialize()
        configRecyclerView()
        createView()
    }

    private fun initialize() {
        recyclerView = findViewById(R.id.activity_postdetail_recycler_view)
    }

    private fun configRecyclerView(){
        recyclerView.hasFixedSize()
        val linearLayoutManager = LinearLayoutManager(this)
        postList = mutableListOf()
        postAdapter =  PostAdapter(this,postList)
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.adapter = postAdapter
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun createView(){
        postDTO?.let { postList.add(it) }
        postAdapter?.notifyDataSetChanged()
    }

}