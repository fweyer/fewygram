package de.fewycoding.fewygram.screen.activity

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.squareup.picasso.Picasso
import com.squareup.picasso.RequestCreator
import de.fewycoding.fewygram.R
import de.fewycoding.fewygram.domain.UserDto
import de.fewycoding.fewygram.domain.UserStoryDto
import de.fewycoding.fewygram.network.firebase.auth.FirebaseAuthService
import de.fewycoding.fewygram.network.firebase.database.FirebaseStoryService
import de.fewycoding.fewygram.screen.dialog.DialogFactory
import de.hdodenhof.circleimageview.CircleImageView
import jp.shts.android.storiesprogressview.StoriesProgressView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class StoryActivity : AppCompatActivity(), StoriesProgressView.StoriesListener {

    companion object {
        const val USER_STORY: String = "USER_STORY"
        const val USER_DTO: String = "USER_DTO"
    }

    private lateinit var storiesProgressView: StoriesProgressView
    private lateinit var imageView : ImageView
    private lateinit var textViewUser : TextView
    private lateinit var profilImage : CircleImageView
    private lateinit var deleteButton : Button
    private lateinit var reverseView : View
    private lateinit var skipView : View
    private lateinit var pauseView: View

    private var loadingPictures = mutableListOf<RequestCreator>()

    private var userStoryDto : UserStoryDto? = null
    private var userDto : UserDto? = null
    private var currentIndex = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_story)
        userStoryDto = intent.getParcelableExtra(USER_STORY)
        userDto =  intent.getParcelableExtra(USER_DTO)
        configLoadingPictures(userStoryDto)
        initialize()
        configPauseAndResume()
        setUserInformation(userDto)
        initStoryProgressView()
    }

    private fun initialize() {
        imageView = findViewById(R.id.activity_story_imageview)
        deleteButton = findViewById(R.id.activity_story_delete)
        textViewUser =  findViewById(R.id.activity_story_textview)
        reverseView = findViewById(R.id.activity_story_reverse)
        skipView = findViewById(R.id.activity_story_skip)
        profilImage = findViewById(R.id.activity_story_profileImage)
        pauseView = findViewById(R.id.activity_story_pause)
        deleteButton.setOnClickListener { onDeleteStoryClick() }
        Picasso.get().load(userStoryDto?.storyDtoList?.get(currentIndex)?.imageURL).into(imageView)
    }

    private fun configLoadingPictures(userStoryDto : UserStoryDto?){
        if (userStoryDto?.storyDtoList == null) return
        for (story in userStoryDto.storyDtoList){
            loadingPictures.add(Picasso.get().load(story.imageURL))
        }
    }

    private fun setUserInformation(userDto: UserDto?) {
        userDto?.let {
            Picasso.get().load(userDto.imageURL).into(profilImage)
            textViewUser.text = userDto.username
        }

        if (userDto?.uuid == FirebaseAuthService.getCurrentUser()?.uid)
            deleteButton.visibility = View.VISIBLE
    }


    private fun initStoryProgressView() {
        val progress = findViewById<StoriesProgressView>(R.id.activity_story_progress)
        storiesProgressView = progress
        userStoryDto?.storyDtoList?.let { storiesProgressView.setStoriesCount(it.size) }
        storiesProgressView.setStoryDuration(6000L)
        storiesProgressView.setStoriesListener(this)
        storiesProgressView.startStories()
        reverseView.setOnClickListener { storiesProgressView.reverse() }
        skipView.setOnClickListener { storiesProgressView.skip() }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun configPauseAndResume (){
        pauseView.setOnTouchListener(View.OnTouchListener { _ , motionEvent ->
            when (motionEvent.action){
                MotionEvent.ACTION_DOWN -> { storiesProgressView.pause()}
                MotionEvent.ACTION_UP -> { storiesProgressView.resume() }
            }
            return@OnTouchListener true
        })
    }

    override fun onNext() {
        GlobalScope.launch  (Dispatchers.Main) {
            loadingPictures[++currentIndex].into(imageView)
        }
    }

    override fun onPrev() {
        if (currentIndex > 0){
            GlobalScope.launch  (Dispatchers.Main) {
                loadingPictures[--currentIndex].into(imageView)
            }
        }
    }

    private fun onDeleteStoryClick() {
        storiesProgressView.pause()
        val onPositivButonClick = { deleteStory() }
        val onNegativButonClick = { storiesProgressView.resume() }
        DialogFactory.createPromtDialog(
            this,"Soll diese Story Bild gelöscht werden?" ,onPositivButonClick,onNegativButonClick)
            .show()
    }

    private fun deleteStory() {
        val onSuccess = {
            Toast.makeText(this,"Story Bild erfolgreich gelöscht",Toast.LENGTH_LONG).show()
            storiesProgressView.skip()
        }
        val onError = { message: String ->
            Toast.makeText(this,message,Toast.LENGTH_LONG).show()
            storiesProgressView.resume()
        }
        FirebaseStoryService.deleteStory(
            userStoryDto?.storyDtoList?.get(currentIndex),
            onSuccess,
            onError
        )
    }

    override fun onComplete() {
        finish()
    }

    override fun onDestroy() {
        storiesProgressView.destroy()
        super.onDestroy()
    }

    override fun onPause() {
        storiesProgressView.pause()
        super.onPause()
    }

    override fun onResume() {
        storiesProgressView.resume()
        super.onResume()
    }
}