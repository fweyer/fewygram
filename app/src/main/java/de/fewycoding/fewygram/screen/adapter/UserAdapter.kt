package de.fewycoding.fewygram.screen.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import de.fewycoding.fewygram.R
import de.fewycoding.fewygram.domain.UserDto
import de.fewycoding.fewygram.screen.activity.MainActivity
import de.fewycoding.fewygram.screen.activity.UserProfileActivity
import de.hdodenhof.circleimageview.CircleImageView

class UserAdapter (
    private var mContext : Context,
    private var mUserDtoList : List<UserDto>,
) : RecyclerView.Adapter<UserAdapter.ViewHolder>()
{
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.user_item, parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       val userDto = mUserDtoList[position]
       holder.usernameTextview.text = userDto.username
       holder.fullnameTextview.text = userDto.fullname
       setProfileImage(holder,userDto.imageURL)
       configView(holder,userDto)
    }

    override fun getItemCount(): Int {
        return mUserDtoList.size
    }

    private fun setProfileImage(holder: ViewHolder, imageURL: String?){
        Picasso.get().load(imageURL).into(holder.imageView)
    }

    private fun configView(holder: ViewHolder, userDto: UserDto){
        holder.itemView.setOnClickListener {
            moveToUserProfileActivity(userDto)
        }
    }

    private fun moveToUserProfileActivity(userDto: UserDto?){
        val bundle = Bundle()
        bundle.putParcelable(UserProfileActivity.USER_ID, userDto)
        val intent = Intent(mContext, UserProfileActivity::class.java)
        intent.putExtras(bundle)
        startActivity(mContext,intent,bundle)
        if (mContext.javaClass != MainActivity::class.java){
            (mContext as Activity).finish()
        }
    }

   inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        val usernameTextview: TextView = itemView.findViewById(R.id.user_item_textview_username)
        val fullnameTextview: TextView = itemView.findViewById(R.id.user_item_textview_fullname)
        val imageView: CircleImageView = itemView.findViewById(R.id.user_item_profileimage)
    }
}