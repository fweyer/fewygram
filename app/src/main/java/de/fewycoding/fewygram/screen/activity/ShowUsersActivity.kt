package de.fewycoding.fewygram.screen.activity

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.fewycoding.fewygram.R
import de.fewycoding.fewygram.domain.SearchArgument
import de.fewycoding.fewygram.domain.SearchDto
import de.fewycoding.fewygram.domain.UserDto
import de.fewycoding.fewygram.network.firebase.database.FirebaseUserService
import de.fewycoding.fewygram.screen.adapter.UserAdapter

class ShowUsersActivity : AppCompatActivity() {

    companion object {
        const val SEARCH_DTO : String = "SEARCH_DTO"
    }

    private lateinit var recyclerView: RecyclerView
    private lateinit var usersList : MutableList<UserDto>

    private var userAdapter: UserAdapter? = null
    private var searchDto : SearchDto? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_users)
        searchDto = intent.getParcelableExtra(SEARCH_DTO)
        initialize()
        configRecyclerView()
        searchDto?.let { reciveData(it) }
    }

    private fun initialize() {
        recyclerView = findViewById(R.id.activity_show_users_recylerview)
    }

    private fun configRecyclerView(){
        recyclerView.hasFixedSize()
        val linearLayoutManager = LinearLayoutManager(this)
        usersList = mutableListOf()
        userAdapter =  UserAdapter(this,usersList)
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.adapter = userAdapter
    }

    private fun reciveData(searchDto: SearchDto){
        when(searchDto.searchArgument){
            SearchArgument.SHOW_LIKES_USERS -> { fetchLikes(searchDto.searchID) }
            SearchArgument.SHOW_FOLLOWER_USERS -> { fetchFollowers(searchDto.searchID) }
            SearchArgument.SHOW_FOLLOWING_USERS -> { fetchFollowing(searchDto.searchID) }
        }
    }

    private fun fetchLikes(postID: String) {
        FirebaseUserService.fetchUsersByLikes(postID){
            updateView(it)
        }
    }

    private fun fetchFollowers(userID: String) {
        FirebaseUserService.fetchUsersByFollowers(userID) {
            updateView(it)
        }
    }

    private fun fetchFollowing(userID: String) {
        FirebaseUserService.fetchUsersByFollowing(userID) {
            updateView(it)
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun updateView(userDtos : List<UserDto>){
        usersList.addAll(userDtos)
        userAdapter?.notifyDataSetChanged()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}

