package de.fewycoding.fewygram.screen.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.fewycoding.fewygram.R
import de.fewycoding.fewygram.domain.PostDto
import de.fewycoding.fewygram.domain.UserStoryDto
import de.fewycoding.fewygram.network.firebase.database.FirebasePostService
import de.fewycoding.fewygram.network.firebase.database.FirebaseStoryService
import de.fewycoding.fewygram.screen.adapter.PostAdapter
import de.fewycoding.fewygram.screen.adapter.StoryAdapter

class HomeFragment : Fragment() {

    private lateinit var recyclerViewPost: RecyclerView
    private lateinit var recyclerViewStory: RecyclerView

    private lateinit var postList : MutableList<PostDto>
    private var postAdapter: PostAdapter? = null
    private var storyAdapter: StoryAdapter? = null
    private lateinit var userStoryDtoList : MutableList<UserStoryDto>


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        initializeView(view)
        configRecyclerViewPost()
        configRecyclerViewStory()
        retrivePostData()
        retriveStoryData()
        return view
    }

    private fun initializeView(view : View){
        recyclerViewPost = view.findViewById(R.id.home_fragment_recyclerview)
        recyclerViewStory = view.findViewById(R.id.home_fragment_recyclerview_story)
    }

    private fun configRecyclerViewPost(){
        val linearLayoutManager = LinearLayoutManager(context)
        linearLayoutManager.reverseLayout = true
        linearLayoutManager.stackFromEnd = true
        recyclerViewPost.layoutManager = linearLayoutManager
        postList = ArrayList()
        postAdapter = context?.let { PostAdapter(it,postList as ArrayList<PostDto>) }
        recyclerViewPost.adapter = postAdapter
    }

    private fun configRecyclerViewStory(){
        val linearLayoutManager = LinearLayoutManager(context)
        linearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
        recyclerViewStory.layoutManager = linearLayoutManager
        userStoryDtoList = ArrayList()
        storyAdapter = context?.let { StoryAdapter(it,userStoryDtoList as ArrayList<UserStoryDto>) }
        recyclerViewStory.adapter = storyAdapter
    }

    private fun retrivePostData(){
        val onComplition = { posts : List<PostDto> -> updatePostView(posts)}
        FirebasePostService.fetchPost(onComplition)
    }

    private fun retriveStoryData() {
        val onComplition = { storyDtos : List<UserStoryDto> -> updateStoryView(storyDtos)}
        FirebaseStoryService.fetchStorys(onComplition)
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun updateStoryView(storyDtos: List<UserStoryDto>){
        userStoryDtoList.clear()
        addPlaceHolderForAddStory()
        userStoryDtoList.addAll(storyDtos)
        storyAdapter?.notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun updatePostView(posts : List<PostDto>){
        postList.clear()
        postList.addAll(posts)
        postAdapter?.notifyDataSetChanged()
    }

    private fun addPlaceHolderForAddStory(){
        userStoryDtoList.add(UserStoryDto())
    }
}