package de.fewycoding.fewygram.screen.activity

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import de.fewycoding.fewygram.R
import de.fewycoding.fewygram.network.firebase.auth.FirebaseAuthService
import de.fewycoding.fewygram.screen.dialog.DialogFactory
import de.fewycoding.fewygram.screen.dialog.components.ProgressDialog

class LoginActivity : AppCompatActivity() {

    private lateinit var progressDialog : ProgressDialog
    private lateinit var registerBtn : Button
    private lateinit var loginBtn : Button
    private lateinit var emailEditText: EditText
    private lateinit var passwortEditText: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initialize()
        initProgressDialog()
    }

    override fun onStart() {
        super.onStart()
        if (FirebaseAuthService.isUserAlreadyLogIn())
            navigateToMainActivity()
    }

    private fun initialize(){
        emailEditText = findViewById(R.id.loginactivity_edittext_email)
        passwortEditText = findViewById(R.id.loginactivity_edittext_passwort)
        registerBtn = findViewById(R.id.loginactivity_btn_register)
        loginBtn = findViewById(R.id.loginactivity_btn_login)
        loginBtn.setOnClickListener { onLoginButtonClick() }
        registerBtn.setOnClickListener { navigateToRegisterActivity() }
    }

    private fun initProgressDialog(){
        val onBackPressedAction = { finish() }
        progressDialog = DialogFactory.createProgrssDialog(
            this, "Login", "Bitte Warten...", onBackPressedAction
        )
    }

    private fun onLoginButtonClick(){
        val email = emailEditText.text.toString()
        val password = passwortEditText.text.toString()

        when {
            TextUtils.isEmpty(email) -> Toast.makeText(this, "Bitte geben Sie die Email ein", Toast.LENGTH_LONG).show()
            TextUtils.isEmpty(password) -> Toast.makeText(this, "Bitte geben Sie das Passwort ein", Toast.LENGTH_LONG).show()
            else -> {
                progressDialog.show(600,600)
                login(email,password)
            }
        }
    }

    private fun login(email: String, password: String) {
        val onSuccess =  { navigateToMainActivity() }
        val onError =  { it: String -> showErrorToast(it) }
        FirebaseAuthService.loginUser(email,password, onSuccess,onError)
    }

    private fun navigateToRegisterActivity(){
        startActivity(Intent(this, RegisterActivity::class.java))
    }

    private fun navigateToMainActivity(){
        progressDialog.dismiss()
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    private fun showErrorToast(message: String){
        progressDialog.dismiss()
        Toast.makeText(this, message,Toast.LENGTH_LONG).show()
    }
}