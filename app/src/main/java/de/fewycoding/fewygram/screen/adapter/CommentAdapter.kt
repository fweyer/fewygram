package de.fewycoding.fewygram.screen.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import de.fewycoding.fewygram.R
import de.fewycoding.fewygram.domain.CommentDto
import de.fewycoding.fewygram.network.firebase.database.FirebaseUserService
import de.hdodenhof.circleimageview.CircleImageView

class CommentAdapter(
    private var mContext: Context,
    private var mCommentDtoList: List<CommentDto>,
) : RecyclerView.Adapter<CommentAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.comments_item,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.commentMessage.text = mCommentDtoList[position].comment
        configUser(holder,mCommentDtoList[position].publisher)
    }

    override fun getItemCount(): Int {
       return mCommentDtoList.size
    }

    private fun configUser(holder: ViewHolder, publisherId : String?){
        FirebaseUserService.fetchUser(publisherId) { user ->
            holder.commentPublisher.text = user?.username
            Picasso.get().load(user?.imageURL).into(holder.profileImageView)
        }
    }

    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        val profileImageView: CircleImageView = itemView.findViewById(R.id.comments_item__profile_image)
        val commentPublisher: TextView = itemView.findViewById(R.id.comments_item_user_name)
        val commentMessage: TextView = itemView.findViewById(R.id.comments_item_message)
    }

}