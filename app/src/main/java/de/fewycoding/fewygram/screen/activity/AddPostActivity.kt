package de.fewycoding.fewygram.screen.activity

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import de.fewycoding.fewygram.R
import de.fewycoding.fewygram.network.firebase.storage.FirebaseStorageService
import de.fewycoding.fewygram.screen.dialog.DialogFactory
import de.fewycoding.fewygram.screen.dialog.components.ProgressDialog

class AddPostActivity : AppCompatActivity() {

    private lateinit var imageView : ImageView
    private lateinit var editText: EditText
    private lateinit var uploadButton : Button
    private lateinit var progressDialog : ProgressDialog

    private var isImageSelected : Boolean = false
    private var imageURI : Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_post)
        initialize()
        initProgressDialog()
        cropImage()
    }

    private fun initialize(){
        imageView = findViewById(R.id.addpostactivity_image_post)
        editText = findViewById(R.id.addpostactivity_edittext_post)
        uploadButton = findViewById(R.id.addpostactivity_button_uploadPost)
        imageView.setOnClickListener { cropImage() }
        uploadButton.setOnClickListener { uploadImage() }
    }

    private fun initProgressDialog(){
        val onBackPressedAction = { finish() }
        progressDialog = DialogFactory.createProgrssDialog(
            this, "Bild wird Hochgeladen", "Bitte Warten...", onBackPressedAction
        )
    }


    private fun uploadImage(){
        if (!isInputValid()){
            Toast.makeText(this, "Bitte Bild und Text hinzufügen",Toast.LENGTH_LONG).show()
            return
        }

        progressDialog.show(800,600)
        imageURI?.let {
            val onSuccess = { succesfulUpdate() }
            val onError = { message: String -> onErrorUploadImage(message) }
            FirebaseStorageService.uploadPostImage(imageURI!!,editText.text.toString(),onSuccess,onError)

        }
    }

    private fun isInputValid() : Boolean{
        return when {
            TextUtils.isEmpty(editText.text) ->  false
            imageURI == null -> return false
            else ->  true
        }
    }

    private fun cropImage(){
        CropImage.activity()
            .setAspectRatio(1,1)
            .setGuidelines(CropImageView.Guidelines.ON)
            .start(this@AddPostActivity)
    }

    private fun succesfulUpdate(){
        Toast.makeText(this, "Post erfolgreich hochgeladen",Toast.LENGTH_LONG).show()
        progressDialog.dismiss()
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    private fun onErrorUploadImage(message : String){
        progressDialog.dismiss()
        Toast.makeText(this, message,Toast.LENGTH_LONG).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE &&
            resultCode == Activity.RESULT_OK &&
            data != null)
        {
            val result = CropImage.getActivityResult(data)
            imageURI = result.uri
            imageView.setImageURI(imageURI)
            isImageSelected = true
        }
        else
        {
            finish()
        }
    }
}