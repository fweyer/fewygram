package de.fewycoding.fewygram.screen.dialog.components

import android.app.AlertDialog
import android.content.Context
import android.view.View

class PromtDialog(
    context: Context,
    title: String,
    onPositivButtonClick: () -> Unit,
    onNegativButtonClick: (() -> Unit)?,
) {

    private var dialog : AlertDialog

    init {
        val builder = AlertDialog.Builder(context)
        builder.setCancelable(true)
        builder.setTitle(title)
        builder.setPositiveButton("Ja") { dialogInterface, _ ->
            onPositivButtonClick()
            dialogInterface.dismiss()
        }
        builder.setNegativeButton("Nein") { dialogInterface, _ ->
            if (onNegativButtonClick != null) {
                onNegativButtonClick()
            }
            dialogInterface.dismiss()
        }
        dialog = builder.create()
    }

    fun show(){
        dialog.show()
    }


}
