package de.fewycoding.fewygram.screen.dialog.components

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.widget.EditText
import de.fewycoding.fewygram.R

class PromtDialogDeleteUser(
    context: Context,
    onPositivButtonClick: (inputText :String) -> Unit,
    onNegativButtonClick: (() -> Unit)?)
{
    private var dialog : AlertDialog

    init {
        val builder = AlertDialog.Builder(context)
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_delete, null, false)
        builder.setCancelable(true)
        val editText = view.findViewById<EditText>(R.id.dialog_delete_editText)
        builder.setView(view)
        builder.setPositiveButton("bestätigen") { dialogInterface, _ ->
            if (editText.text.isNotEmpty()){
                onPositivButtonClick(editText.text.toString())
            }
            dialogInterface.dismiss()
        }
        builder.setNegativeButton("abbrechen") { dialogInterface, _ ->
            if (onNegativButtonClick != null) {
                onNegativButtonClick()
            }
            dialogInterface.dismiss()
        }
        dialog = builder.create()
    }

    fun show(){
        dialog.show()
    }

}
