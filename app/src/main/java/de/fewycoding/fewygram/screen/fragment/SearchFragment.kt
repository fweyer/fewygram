package de.fewycoding.fewygram.screen.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.fewycoding.fewygram.R
import de.fewycoding.fewygram.screen.adapter.UserAdapter
import de.fewycoding.fewygram.domain.UserDto
import de.fewycoding.fewygram.network.firebase.database.FirebaseUserService

class SearchFragment : Fragment() {

    private lateinit var recyclerViewUsers : RecyclerView
    private lateinit var mUserDtoList: MutableList<UserDto>
    private lateinit var searchText : EditText

    private var userAdapter: UserAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_search, container, false)
        initializeView(view)
        configRecylerView(view)
        return view
    }

    private fun initializeView(view: View){
        searchText = view.findViewById(R.id.fragment_search_edittext_searchtext)
        searchText.addTextChangedListener( setTextWatcher())
    }

    private fun configRecylerView(view: View) {
        recyclerViewUsers = view.findViewById(R.id.fragment_search_recyclerview)
        recyclerViewUsers.hasFixedSize()
        recyclerViewUsers.layoutManager = LinearLayoutManager(context)
        mUserDtoList = mutableListOf()
        userAdapter = context?.let { UserAdapter(it,mUserDtoList)}
        recyclerViewUsers.adapter = userAdapter
    }

    private fun setTextWatcher() : TextWatcher {
        return object :TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun afterTextChanged(s: Editable?) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (TextUtils.isEmpty(searchText.text)){
                    hideSearchResults()
                } else{
                    val onComplition = { it: MutableList<UserDto> -> displaySearchResults(it) }
                    FirebaseUserService.searchUsers(s?.toString(),onComplition)
                }
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun displaySearchResults(userDtoList: MutableList<UserDto>){
        mUserDtoList.clear()
        recyclerViewUsers.visibility = View.VISIBLE
        mUserDtoList.addAll(userDtoList)
        userAdapter?.notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun hideSearchResults(){
        mUserDtoList.clear()
        recyclerViewUsers.visibility = View.GONE
        userAdapter?.notifyDataSetChanged()
    }
}