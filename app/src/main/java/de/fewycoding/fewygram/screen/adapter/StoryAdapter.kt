package de.fewycoding.fewygram.screen.adapter

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import de.fewycoding.fewygram.R
import de.fewycoding.fewygram.domain.UserDto
import de.fewycoding.fewygram.domain.UserStoryDto
import de.fewycoding.fewygram.network.firebase.database.FirebaseUserService
import de.fewycoding.fewygram.screen.activity.AddStoryActivity
import de.fewycoding.fewygram.screen.activity.StoryActivity
import de.hdodenhof.circleimageview.CircleImageView

class StoryAdapter (
    private var mContext : Context,
    private var mUserStoryDtoList : List<UserStoryDto>,
) : RecyclerView.Adapter<StoryAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return if (viewType == 0){
            val view = LayoutInflater.from(mContext).inflate(R.layout.add_story_item,parent,false)
            ViewHolder(view)
        } else {
            val view = LayoutInflater.from(mContext).inflate(R.layout.story_item,parent,false)
            ViewHolder(view)
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when(holder.itemViewType){
            0 -> configStoryAddItem(holder)
            1 -> configStoryItem(holder,position)
        }
    }

    private fun configStoryAddItem(holder: ViewHolder) {
        FirebaseUserService.fetchCurrentUser { user ->
            Picasso.get().load(user?.imageURL).into(holder.addStoryImage)
        }

        holder.addStoryImage.setOnClickListener {
            val intent = Intent(mContext, AddStoryActivity::class.java)
            mContext.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return mUserStoryDtoList.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) 0 else 1
    }

    private fun configStoryItem(holder: ViewHolder, position: Int){
        val userStory = mUserStoryDtoList[position]
        FirebaseUserService.fetchUser(userStory.userId){ user ->
            holder.storyTextView.text = user?.username ?: "Benutzer"
            Picasso.get().load(user?.imageURL).placeholder(R.drawable.profile).into(holder.storyImageNew)
            configClickListener(holder,userStory,user)
        }
    }

    private fun configClickListener(holder: ViewHolder, userStoryDto: UserStoryDto, userDto: UserDto?) {
        holder.storyImageNew.setOnClickListener {
            val bundle = Bundle()
            bundle.putParcelable(StoryActivity.USER_STORY, userStoryDto)
            bundle.putParcelable(StoryActivity.USER_DTO, userDto)
            val intent = Intent(mContext, StoryActivity::class.java)
            intent.putExtras(bundle)
            ContextCompat.startActivity(mContext, intent, bundle)
        }
    }

    // Typen dürfen nicht explicit angegeben werden. Sorgt für einen Nullpointer. Mehrere ViewTypes können damit nicht umgehen
    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        val addStoryImage = itemView.findViewById<CircleImageView>(R.id.add_story_item_imageview)
        val storyImageNew = itemView.findViewById<CircleImageView>(R.id.story_item_image_new)
        val storyTextView = itemView.findViewById<TextView>(R.id.story_item_username)
    }
}