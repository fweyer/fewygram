package de.fewycoding.fewygram.screen.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import de.fewycoding.fewygram.R
import de.fewycoding.fewygram.domain.PostDto
import de.fewycoding.fewygram.domain.SearchArgument
import de.fewycoding.fewygram.domain.SearchDto
import de.fewycoding.fewygram.domain.UserDto
import de.fewycoding.fewygram.network.firebase.database.FirebaseCommentsService
import de.fewycoding.fewygram.network.firebase.database.FirebaseLikesService
import de.fewycoding.fewygram.network.firebase.database.FirebaseUserService
import de.fewycoding.fewygram.screen.activity.CommentsActivity
import de.fewycoding.fewygram.screen.activity.ShowUsersActivity
import de.hdodenhof.circleimageview.CircleImageView

class PostAdapter (
    private var mContext : Context,
    private var mPostList : List<PostDto>,
    ) : RecyclerView.Adapter<PostAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostAdapter.ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.posts_item,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: PostAdapter.ViewHolder, position: Int) {
        val postDto = mPostList[position]
        fetchPublisherInfo(holder,postDto.publisher)
        Picasso.get().load(postDto.postImageURL).into(holder.postImage)
        holder.description.text = postDto.description
        configLikeButton(holder,postDto.postID)
        configOnViewClick(holder, postDto)
        configCommentsCount(holder, postDto.postID)
        configLikes(holder,postDto.postID)
    }

    override fun getItemCount(): Int {
        return mPostList.size
    }

    private fun fetchPublisherInfo(holder: ViewHolder, puplisherID: String?){
        val onComplition =  { userDto : UserDto? -> setPublisherInfo(userDto,holder)  }
        FirebaseUserService.fetchUser(puplisherID, onComplition)
    }

    private fun setPublisherInfo(userDto: UserDto?, holder: PostAdapter.ViewHolder) {
        userDto?.apply {
            Picasso.get().load(imageURL).into(holder.profileImage)
            holder.username.text = username
            holder.publisher.text = fullname
        }
    }

    private fun configCommentsCount(holder: PostAdapter.ViewHolder, postID: String?){
        val onComplition = { size : Int? -> holder.commentsCount.text = setCommentsCount(size) }
        FirebaseCommentsService.countComments(postID,onComplition)
    }

    private fun configLikeButton(holder: PostAdapter.ViewHolder, postID: String? ){
        setLikeCount(holder, postID)

        val configLikeInitial = { isLike : Boolean -> setLikeIcon(isLike, holder)  }
        FirebaseLikesService.isPostLiked(postID,configLikeInitial)

        holder.likeButton.setOnClickListener {
            FirebaseLikesService.togglePostLike(postID) {
                isLike : Boolean -> setLikeIcon(isLike, holder)
                setLikeCount(holder, postID)
            }
        }
    }

    private fun setLikeIcon(isLike : Boolean, holder: PostAdapter.ViewHolder){
        if (isLike){
            holder.likeButton.setImageResource(R.drawable.heart_clicked)
        } else {
            holder.likeButton.setImageResource(R.drawable.heart_not_clicked)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setLikeCount(holder: ViewHolder, postID: String?) {
        val onComplition = { size : Int? -> holder.likes.text = "Likes " + size.toString()}
        FirebaseLikesService.countLikes(postID,onComplition)
    }

    private fun configOnViewClick(holder: ViewHolder,  postDto: PostDto){
        holder.itemView.setOnClickListener {
            navigateToCommentsActivity(postDto)
        }
    }

    private fun configLikes(holder: ViewHolder,  postID: String?){
        holder.likes.setOnClickListener { navigateToShowUserActivity(postID) }
    }

    private fun navigateToShowUserActivity(postID: String?) {
        postID?.let {
            val bundle = Bundle()
            val searchDTO = SearchDto(SearchArgument.SHOW_LIKES_USERS, postID)
            bundle.putParcelable(ShowUsersActivity.SEARCH_DTO, searchDTO)
            val intent = Intent(mContext, ShowUsersActivity::class.java)
            intent.putExtras(bundle)
            ContextCompat.startActivity(mContext, intent, bundle)
        }
    }


    private fun setCommentsCount(count : Int?): String {
        return if (count != null && count > 0){
            "Kommentare $count"
        } else{
            ""
        }
    }

    private fun navigateToCommentsActivity(postId : PostDto){
        val intent = Intent(mContext, CommentsActivity::class.java)
        val bundle = Bundle()
        bundle.putParcelable(CommentsActivity.POST_DTO, postId)
        intent.putExtra(CommentsActivity.POST_DTO, postId)
        mContext.startActivity(intent)
    }

    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        val profileImage: CircleImageView = itemView.findViewById(R.id.post_item__profile_image)
        val postImage: ImageView = itemView.findViewById(R.id.post_item_image)
        val likeButton: ImageView = itemView.findViewById(R.id.post_item_like_btn)
        val username: TextView = itemView.findViewById(R.id.post_item_user_name)
        val likes: TextView = itemView.findViewById(R.id.post_item_likes)
        val publisher: TextView = itemView.findViewById(R.id.post_item_publisher)
        val description: TextView = itemView.findViewById(R.id.post_item_description)
        val commentsCount: TextView = itemView.findViewById(R.id.post_item_commentscount)
    }
}