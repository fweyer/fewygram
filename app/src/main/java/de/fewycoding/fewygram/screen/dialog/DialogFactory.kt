package de.fewycoding.fewygram.screen.dialog

import android.content.Context
import android.view.View
import de.fewycoding.fewygram.screen.dialog.components.PromtDialog
import de.fewycoding.fewygram.screen.dialog.components.ProgressDialog
import de.fewycoding.fewygram.screen.dialog.components.PromtDialogDeleteUser
import de.fewycoding.fewygram.screen.dialog.components.ShowImageDialog

object DialogFactory {

    fun createProgrssDialog(context : Context, title: String, message: String,onBackPressAction: () -> Unit)
        = ProgressDialog(context,title,message, onBackPressAction)

    fun createShowImageDialog(context : Context, imageURL : String)
        = ShowImageDialog(context,imageURL)

    fun createPromtDialog(context: Context, title: String, onPositivButtonClick: () -> Unit, onNegativButtonClick: (() -> Unit)?)
        = PromtDialog(context, title, onPositivButtonClick, onNegativButtonClick)

    fun createPromtDialogDeleteUser(context: Context, onPositivButtonClick: (inputText :String) -> Unit, onNegativButtonClick: (() -> Unit)?)
        = PromtDialogDeleteUser(context,onPositivButtonClick,onNegativButtonClick)
}